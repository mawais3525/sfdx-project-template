import { LightningElement , wire} from 'lwc';
import getAccountList from '@salesforce/apex/testtraining.getAccountList';

export default class TrainingLwc extends LightningElement {

 greeting = "Training";
 result;

 connectedCallback(){
    //do something
}  

 handleClick(event){

     this.greeting = event.target.value;
 }
 
 @wire(getAccountList)accountList({error,data}){

    if(data){ 
        this.result=data;
        this.error = undefined;
        window.console.log('this is'+ JSON.stringify(data));
    } else if(error){
        this.error=error;
        this.result=undefined;
        window.console.log('this is error '+ JSON.stringify(error));
    }
 }


}