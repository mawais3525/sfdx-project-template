import { LightningElement, track, wire } from 'lwc';
import { MessageContext, subscribe, APPLICATION_SCOPE, unsubscribe } from 'lightning/messageService';
import COUNT_UPDATED_CHANNEL from '@salesforce/messageChannel/CountUpdated__c';
import sendMail from '@salesforce/apex/EmailMissionSpecialist.sendMail';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';


export default class SendEmail extends LightningElement {

    @wire(MessageContext)
    messageContext;
    subscription = null;
    @track s='';
    @track sObjName;
    @track sObjEmail = '';
    @track sObjBody;
    @track errorMsg;
 
    emailHandleChange(event){
        if(event.target.name == 'sName'){
        this.sObjName = event.target.value;  
        }
        if(event.target.name == 'sBody'){
        this.sObjBody = event.target.value;
        }

    }

    submitAction(){
    sendMail({address:this.sObjEmail,subject:this.sObjName,body:this.sObjBody})
    .then(result=>{      
        const toastEvent = new ShowToastEvent({
            title:'Success!',
            message:'Email sent successfully',
            variant:'success'
            });
            this.dispatchEvent(toastEvent);

    })
    .catch(error =>{
        this.errorMsg=error.message;
    });

    }
    subscribeToMessageChannel() {
        this.subscription = subscribe(
          this.messageContext,
          COUNT_UPDATED_CHANNEL,
          (message) => this.handleMessage(message)
        );
      }
      handleMessage(message) {
        this.sObjEmail= message.emailAddress;

      }
      connectedCallback() {
        this.subscribeToMessageChannel();
      }


}