import { LightningElement, wire, api, track} from 'lwc';
import fetchAcc from '@salesforce/apex/AccRelatedConC.fetchAcc';
import { NavigationMixin } from "lightning/navigation";
import { deleteRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
export default class RelateContact extends NavigationMixin(LightningElement) {

    data1 = [];
    wiredActivities;
    records = '0';
    error;
    @api recordId;
    @api objectApiName;
    @track error;
    sfdcBaseURL;

    renderedCallback() {
        this.sfdcBaseURL = window.location.origin + "/lightning/r/Account/" + this.recordId + "/related/Contacts/view";
        console.log(this.sfdcBaseURL);
    }
    @wire(fetchAcc, {
        RecId: '$recordId'
    })
    wiredclass({
        data, error

    }) {
        if (data) {
            let dataEditing = JSON.parse(JSON.stringify(data));
            //console.log(JSON.stringify(dataEditing));
            this.records = dataEditing.length;
            this.data1 = dataEditing;

        } else if (error) {
            this.error = error;
        }

    }

    handleNavigateToNew(event) {
        let targetId = event.target.dataset.id;
        this[NavigationMixin.Navigate]({
            type: 'standard__recordRelationshipPage',
            attributes: {
                recordId: targetId,
                objectApiName: 'Account', 
                relationshipApiName: 'AccountContactRelation', 
                //actionName: 'view' 
                //objectApiName: "Contact", 
                actionName: 'new',
            },
        });
    }

    handleNavigate(event) {
        let targetId = event.target.dataset.id;
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: targetId,
                objectApiName: "Contact", // pass the record id here.
                actionName: 'edit',
            },
        });
    }

    deleteContact(event) {
        let targetId = event.target.dataset.id;
        deleteRecord(targetId)
        .then(() => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success',
                    message: 'Record deleted',
                    variant: 'success'
                })
            );
        })
        .catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error deleting record',
                    message: error.body.message,
                    variant: 'error'
                })
            );
        });
    }


}