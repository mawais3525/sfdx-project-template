import { LightningElement , track , wire} from 'lwc';
import { publish, MessageContext } from 'lightning/messageService';
import COUNT_UPDATED_CHANNEL from '@salesforce/messageChannel/CountUpdated__c';
import getCustomLookupAccount from '@salesforce/apex/returnSelectedContact.getCustomLookupAccount';
export default class ContactLookup extends LightningElement {

    @wire(MessageContext)
    messageContext;

 @track contactName='';
 @track contactList=[];
 @track objectApiName='Contact';
 @track emailAddress='';
 @track isShow=false;
 @track messageResult=false;
 @track isShowResult = true;
 @track showSearchedValues = false;
 @wire(getCustomLookupAccount,{actName:'$contactName'})
 retrieveAccounts ({error,data}){
     this.messageResult=false;
     if(data){
         if(data.length>0 && this.isShowResult){
            this.contactList =data;
            this.showSearchedValues=true;
            this.messageResult=false;
         }
         else if(data.length == 0){
            this.contactList=[];
            this.showSearchedValues=false;
            if(this.contactName != ''){
               this.messageResult=true;
            }
         }
         else if(error){
             this.emailAddress='';
             this.contactName='';
             this.contactList=[];
             this.showSearchedValues=false;
             this.messageResult=true;
         }
 
     }
 }

 searchHandleClick(event){
  this.isShowResult = true;
  this.messageResult = false;
}
 
 
searchHandleKeyChange(event){
  this.messageResult=false;
  this.contactName = event.target.value;
}
 
parentHandleAction(event){        
    this.showSearchedValues = false;
    this.isShowResult = false;
    this.messageResult=false;
    this.emailAddress =  event.target.dataset.value;
    //const payload = { email: emailAddress };
    console.log('emailAddress::'+this.emailAddress);    
    // const selectedEvent = new CustomEvent('selected', { detail: this.emailAddress });
    // this.dispatchEvent(selectedEvent);
    const payload = { 
        emailAddress: this.emailAddress
      };    
    publish(this.messageContext, COUNT_UPDATED_CHANNEL, payload);

 }
 
}