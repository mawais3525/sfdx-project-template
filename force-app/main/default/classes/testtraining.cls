public with sharing class testtraining {
    public testtraining() {

    }

    @AuraEnabled(cacheable = true)
    public static List<Account> getAccountList(){
        try {
            return [Select Id, Name From Account];
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}