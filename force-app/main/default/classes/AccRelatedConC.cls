public with sharing class AccRelatedConC {

    @AuraEnabled(cacheable=false)
    public static List<Account> fetchAcc (id RecId){
        string query= 'SELECT Id, Name, Phone, Title, Email, Active_Contact__c ,AccountId FROM Contact'+
                '  WHERE AccountId = :RecId And Active_Contact__c = true';
        
         return Database.query( query );
        
    }  
}