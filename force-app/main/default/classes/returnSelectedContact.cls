public with sharing class returnSelectedContact {
 
    @AuraEnabled(cacheable=true)
    public static List<Contact> getCustomLookupAccount (String actName){
       List<Contact> accLookupList =  new List<Contact>();
       if(actName != ''){
           String contactName = '%' + actName + '%';
           accLookupList = [Select Id, Name ,Email From Contact Where Name like:contactName];
           return accLookupList;
       }
       return accLookupList;
    }   

}