public class EmailMissionSpecialist {
    @AuraEnabled
    public static void sendMail(String address, String subject, String body) {
        System.debug(address);
        EmailTemplate et = [SELECT Id FROM EmailTemplate WHERE name ='Test Email Template'];
       Contact primaryContact = [SELECT Id ,name FROM Contact WHERE name ='aa bb'];
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      String[] toAddresses = new String[] {address};
      mail.setToAddresses(toAddresses);
      //mail.setSubject(subject);
      mail.setSaveAsActivity(false);
      mail.setTargetObjectId(UserInfo.getUserId());
      mail.setTemplateId(et.Id);
      Messaging.SendEmailResult[] results = Messaging.sendEmail(
      new Messaging.SingleEmailMessage[] { mail });
      inspectResults(results);
   }
   private static Boolean inspectResults(Messaging.SendEmailResult[] results) {
      Boolean sendResult = true;
      for (Messaging.SendEmailResult res : results) {
         if (res.isSuccess()) {
            System.debug('Email sent successfully');
         }
         else {
            sendResult = false;
            System.debug('The following errors occurred: ' + res.getErrors());                 
         }
      }
      return sendResult;
   }

}